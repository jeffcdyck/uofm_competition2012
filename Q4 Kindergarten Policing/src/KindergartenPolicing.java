/**
 * Created with IntelliJ IDEA.
 * User: Jeff
 * Date: 08/05/14
 * Time: 6:50 PM
 * To change this template use File | Settings | File Templates.
 */
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;
import java.lang.StrictMath.*;

public class KindergartenPolicing
{
    public static void main(String[] args)
    {
        Scanner scanner = new Scanner(System.in);

        int numStudents = 0;
        String numStudentsStr = scanner.nextLine();
        numStudents = Integer.parseInt(numStudentsStr);

        for (int i = 0; i < numStudents; i++)
        {
            List<Integer> students = new ArrayList();
            int max = 0;

            String studentString = scanner.nextLine();
            System.out.println(String.format("Got string: [%s]", studentString));
            String[] strs = studentString.split(" ");
            for (int j = 0; j < strs.length; j++)
            {
                int studentID = Integer.parseInt(strs[j]);
                students.add(studentID);

                if (studentID > max)
                {
                    max = studentID;
                }
            }

            if (students.size() == max)
            {
                System.out.println("No one is missing.");
            }
            else
            {
                findMissingStudentSum(students, max);
                findMissingStudentIterate(students, max);
                findMissingStudentSorting(students, max);
            }
        }
    }

    /*
        Find the missing student by calculating the theoretical
        sum of student IDs from 1-n by n(n+1)/2
        Then take the difference, that's the missing student
     */
    public static void findMissingStudentSum(List<Integer> students, int max )
    {
        int total = 0;
        for (int i = 0; i < students.size(); i++)
        {
            total += students.get(i);
        }

        int theoreticalTotal = (max * (max + 1)) / 2;

        int missing = theoreticalTotal - total;

        System.out.println(String.format("Student %d is missing!", missing));
    }

    /*
        Find the missing student by creating an array of [max] size,
        and "Checking off" each student that's present
        Re-iterate over the array, and find the element that isn't checked
     */
    public static void findMissingStudentIterate(List<Integer> students, int max )
    {
        int shouldBe[] = new int[max+1];

        for (int i = 0; i < students.size(); i++)
        {
            shouldBe[students.get(i)] = 1;
        }

        for (int i = 1; i <= max; i++)
        {
            if (shouldBe[i] == 0)
            {
                System.out.println(String.format("Student %d is missing!!", i));
            }
        }
    }

    /*
        Find the missing student by first sorting the list of children
        Then re-iterate over the array, and the first occurrence where
        stud[j] != stud[j-1]+1, that's the missing element
        i.e. [1 2 4]
        Missing 3 since stud[2] != stud[1]+1
     */
    public static void findMissingStudentSorting(List<Integer> students, int max )
    {
        Collections.sort(students);

        int prev = 0;
        for (int i = 0; i < students.size(); i++)
        {
            if (students.get(i) != prev+1)
            {
                System.out.println(String.format("Student %d is missing!!!", i+1));
                break;
            }
            else
            {
                prev++;
            }
        }
    }
}
