/**
 * Created with IntelliJ IDEA.
 * User: Jeff
 * Date: 08/05/14
 * Time: 1:54 PM
 * To change this template use File | Settings | File Templates.
 */

import java.util.Scanner;

public class CoolingPies
{
    public static void main(String[] args)
    {
        Scanner scanner = new Scanner(System.in);
        int numPies = 0;

        double Tp = 0;
        int Tr = 0;
        double k = 0.0;
        int Q = 0;
        int M = 0;

        numPies = scanner.nextInt();

        for (int i = 1; i <= numPies; i++)
        {
            // Gather pie and room information
            Tp = scanner.nextDouble();
            Tr = scanner.nextInt();
            k = scanner.nextDouble();
            Q = scanner.nextInt();
            M = scanner.nextInt();

            double temp = 0.0;

            for (int mins = 0; mins < M; mins++)
            {
                Tp += (k * (Tp - Tr));
                Tr = Tr + Q;
            }

            System.out.println(String.format("Case %d %f", i, Tp));



        }
    }
}
