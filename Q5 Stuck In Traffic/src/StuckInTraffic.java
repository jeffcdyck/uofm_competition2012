/**
 * Created with IntelliJ IDEA.
 * User: Jeff
 * Date: 08/05/14
 * Time: 7:43 PM
 * To change this template use File | Settings | File Templates.
 */
import java.util.Scanner;
import java.lang.StrictMath.*;

public class StuckInTraffic
{
    public static void main(String[] args)
    {
        Scanner scanner = new Scanner(System.in);
        int numLines = 0;
        String numLinesStr = scanner.nextLine();
        numLines = Integer.parseInt(numLinesStr);

        for (int i = 0; i < numLines; i++)
        {
            int numCars = 0;

            String line = scanner.nextLine();
            char[] spots = line.toCharArray();

            numCars = spots.length;

            for (int j = numCars-1; j >= 0; j--)
            {
                // System.out.println("Checking car" + j);
                int speed = 0;
                char car = spots[j];
                if (car != '_')
                {
                    speed = Integer.parseInt(car+"");
                    // System.out.println(String.format("Car %d has speed %d", j, speed));
                    if (speed+j > (numCars-1))
                    {
                        spots[j] = '_';
                    }
                    else
                    {
                        spots[j + speed] = car;
                        if (speed != 0)
                        {
                            spots[j] = '_';
                        }
                    }
                }
            }

            StringBuilder sb = new StringBuilder();
            for (int j = 0; j < numCars; j++)
            {
                sb.append(spots[j]);
            }

            System.out.println(sb.toString());
        }
    }
}
