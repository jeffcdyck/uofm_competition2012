/**
 * Created with IntelliJ IDEA.
 * User: Jeff
 * Date: 08/05/14
 * Time: 6:27 PM
 * To change this template use File | Settings | File Templates.
 */
import java.util.Scanner;
public class WinnipegDistance
{
    public static void main(String[] args)
    {
        Scanner scanner = new Scanner(System.in);

        int numCities = 0;
        int cityX = 0;
        int cityY = 0;
        int aX = 0;
        int aY = 0;
        int bX = 0;
        int bY = 0;

        numCities = scanner.nextInt();

        for (int i = 0; i < numCities; i++)
        {
            cityX = scanner.nextInt();
            cityY = scanner.nextInt();
            aX = scanner.nextInt();
            aY = scanner.nextInt();
            bX = scanner.nextInt();
            bY = scanner.nextInt();

            int man = manhattan(aX, aY, bX, bY);
            int win = winnipeg(cityX, cityY, aX, aY, bX, bY);


            String out = String.format("Manhattan: %d, Winnipeg: %d", man, win);
            System.out.println(out);
        }
    }

    public static int manhattan(int aX, int aY, int bX, int bY)
    {
        int val = abs(bX - aX) + abs(bY - aY);

        return val;
    }

    public static int winnipeg(int cityX, int cityY, int aX, int aY, int bX, int bY)
    {
        int man = manhattan(aX, aY, bX, bY);

        int win = distanceToPerimeter(cityX, cityY, aX, aY) +
                  distanceToPerimeter(cityX, cityY, bX, bY);

        return java.lang.StrictMath.min(man, win);
    }

    public static int distanceToPerimeter(int cityX, int cityY, int x, int y)
    {
        int distX = java.lang.StrictMath.min(x, cityX - x);
        int distY = java.lang.StrictMath.min(y, cityY - y);

        return java.lang.StrictMath.min(distX, distY);
    }

    public static int abs(int x)
    {
        if (x > 0) return x;
        else return -x;
    }
}
