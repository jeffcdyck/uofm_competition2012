import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * User: Jeff
 * Date: 08/05/14
 * Time: 1:23 PM
 * To change this template use File | Settings | File Templates.
 */
import java.util.Scanner;

public class LoanPayment
{
    public static void main(String[] args)
    {
        int N = 0;
        int P = 0;
        double r = 0.0;

        Scanner scanner = new Scanner(System.in);

        P = scanner.nextInt();
        N = scanner.nextInt();
        r = scanner.nextDouble();

        double rInc = r + 1.0;
        double raised = myExp(rInc, N);

        double total = (P * r * raised) / (raised - 1);

        System.out.println("The monthly payment is $" + total + ".");


    }

    public static double myExp(double a, int e)
    {
        double val = 1;
        for (int i = 0; i < e; i++)
        {
            val = val * a;
            System.out.println("Val: " + val);
        }

        return val;
    }
}
